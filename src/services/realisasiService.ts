// import interface untuk api
import api from "../helper/apiServices";

// fungsi untuk mengabil data dari setiap end point api
const getData = () => {
  return api.get("/realisasi-baru?");
};

export default {
  getData,
};
