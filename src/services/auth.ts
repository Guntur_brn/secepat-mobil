import api from "../helper/apiServices";

const login = (body: any) => {
    return api.post('/auth/token/issue?include=unit', body)
}

export default {
    login
}