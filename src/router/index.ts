import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [

  {
    path: "/",
    redirect: "/loginView",
  },
  {
    path: "/dashboard",
    component: () => import("../views/Pages/Dasboard.vue"),
  },
  {
    path: '/folder/:id',
    component: () => import ('../views/FolderPage.vue')
  },
  {
    path: "/loginView",
    component: () => import("../views/Pages/LoginView.vue"),
  },
  {
    path: "/panduan",
    component: () => import("../views/Pages/PanduanView.vue"),
  },
  {
    path: "/pengguna",
    component: () => import("../views/Pages/Pengguna.vue"),
  },
  {
    path: "/grade-pd",
    component: () => import("../views/Pages/GradePerPd.vue"),
  },
  {
    path: "/komponen-apbd",
    component: () => import("../views/Pages/KomponenApbd.vue"),
  },
  {
    path: "/rincian-paket-tender",
    component: () => import("../views/Pages/RincianPaketTender.vue"),
  },
  {
    path: "/rincian-paket-nontender",
    component: () => import("../views/Pages/RincianPaketNonTender.vue"),
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
