import api from "../../helper/apiServices"

const getPost = () => {
    return api.get('/posts')
}

const getDetail = (id: any) => {
    return api.get(`/posts/`+id)
}

export default {
    getPost,
    getDetail
}