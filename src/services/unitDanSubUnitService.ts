
import api from "../helper/apiServices";

const getData = () => {
  return api.get("/pengaturan?force-sub-unit=true")
};

export default {
  getData,
};
