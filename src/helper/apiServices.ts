import axios from "axios";
import { BaseUrl } from "./variable";
import { Storage } from '@capacitor/storage';

const instance = axios.create({
  baseURL: BaseUrl,
  headers: { "Access-Control-Allow-Origin": "*" },
});


instance.interceptors.request.use(
  async function (config) {
    const { value } = await Storage.get({ key: 'token' });
    config.headers = {
      'Authorization': `Bearer ${value}`,
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json'
    };
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    return response.data;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default instance;
