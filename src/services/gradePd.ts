import api from "../helper/apiServices";

const getData = () => {
  return api.get("/monev/resume/grade-skpd?jenis_anggaran=pergeseran&tahun=2021&bulan=12");
};

export default {
  getData,
};
