// import interface untuk api
import api from "../helper/apiServices";

// fungsi untuk mengabil data dari setiap end point api
const getData = (params: any) => {
  return api.get("/monev/resume/realisasi-baru", {
    params: params
  });

  
};

export default {
  getData,
};
